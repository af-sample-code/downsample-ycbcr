cmake_minimum_required(VERSION 3.21)

project(color_compression)

add_executable(
    color_compress
    src/main.cpp
    3rdparty/lodepng/lodepng.cpp
    )

target_include_directories(
    color_compress 
    PUBLIC 
    3rdparty
    )
