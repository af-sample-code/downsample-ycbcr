# Purpose

This is a pedagogic example code to show the effect of reducing the resolution of Y', Cb', Cr' channels. Decomposing an R'G'B' image (gamma corrected) into Y'Cb'Cr' and compressing the chroma channels Cb'Cr' is a common way of compressing camera pictures.

This program reads an sRGB PNG image, reduce the resolution of Y' and/or Cb'Cr' channels and display the expected output in another PNG file. It shows that downsampling Y' produces obvious artifacts while downsampling Cb'Cr' channels is almost un-noticeable by human eyes if done on photographs until a certain downsampling factor.


# Compilation

You need CMake to compile this program:

```
mkdir build
cd build
cmake ..
```

Then, you can execute `color_compress` from `build` directory.


# Usage

The executable takes 3 arguments:
- The path to the PNG image to "compress" (no compression is involved, see "Purpose" section).
- The downsampling factor to apply to Y' (luma) channel (set 1 for no downsampling).
- The downsampling factor to apply to Cb' & Cr' (chroma) channels (set 1 for no downsampling).
- The path to the PNG output file.

For example:

```
./color_compress in.png 1 2 out.png
```

This will produce a `out.png` file with no downsampling on Y and half the resolution for chroma channels from `in.png`.

Optionally, you can specify two additional files to store the "compressed" luma chroma channels. These two images will be rescaled at the original image resolution with a bilinear filtering, as done in the "decompression" step:
- The path to the PNG output file for luma.
- The path to the PNG output file for chroma.

For example:

```
./color_compress in.png 1 2 out.png luma.png chroma.png
```

This will produce the same output as the previous command but also outputs `luma.png` and `chroma.png` respectively storing the luma (Y') and chroma channels (Cb' & Cr' with Y = 0.5 / 128).


# License

It is licensed under the 3-clause BSD license.