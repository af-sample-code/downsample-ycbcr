/**
 * Copyright (c) 2021 Alban Fichet <alban dot fichet at gmx dot fr>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *  * Neither the name of the organization(s) nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <vector>
#include <cmath>
#include <stddef.h>
#include <assert.h>

#include <lodepng/lodepng.h>

template <typename T>
class Image {
public:
    Image(
        const std::vector<T>& image, 
        size_t width, size_t height,
        size_t downsample = 1,
        size_t n_pixel_samples = 4)
        : m_width(width / downsample)
        , m_height(height / downsample)
        , m_n_pixel_samples(n_pixel_samples)
        , m_image(m_width * m_height * m_n_pixel_samples)
    {
        std::vector<float> rgb_avg(m_n_pixel_samples);

        for (size_t y_target = 0; y_target < m_height; y_target++) {
            for (size_t x_target = 0; x_target < m_width; x_target++) {

                // Average surrounding pixels
                memset(rgb_avg.data(), 0, m_n_pixel_samples * sizeof(float));

                for (size_t y_offset = 0; y_offset < downsample; y_offset++) {
                    const size_t y_org = downsample * y_target + y_offset;

                    assert(y_org < height);

                    for (size_t x_offset = 0; x_offset < downsample; x_offset++) {
                        const size_t x_org = downsample * x_target + x_offset;
    
                        assert(x_org < width);

                        for (size_t c = 0; c < m_n_pixel_samples; c++) {
                            rgb_avg[c] += image[m_n_pixel_samples * (y_org * width + x_org) + c];
                        }
                    }
                }
            
                // Set the target color
                for (size_t c = 0; c < m_n_pixel_samples; c++) {
                    m_image[m_n_pixel_samples * (y_target * m_width + x_target) + c] = rgb_avg[c] / (downsample*downsample);
                } 

            }
        }
    }


    // Retrieve color for pixel in coordinates [0, 1]
    void get_pixel(float u, float v, T value[]) const
    {
        assert(u >= 0.f && u <= 1.f);
        assert(v >= 0.f && v <= 1.f);

        const float fRel_x = u * (m_width - 1);
        const float fMin_x = std::floor(fRel_x);
        const float fMax_x = std::min((size_t)fMin_x + 1, m_width - 1);
        const float fAlpha_x = fRel_x - fMin_x; // 0..1 to interpolate on x

        const float w_x[2] = {1.f - fAlpha_x, fAlpha_x};

        const float fRel_y = v * (m_height - 1);
        const float fMin_y = std::floor(fRel_y);
        const float fMax_y = std::min((size_t)fMin_y + 1, m_height - 1);
        const float fAlpha_y = fRel_y - fMin_y; // 0..1 to interpolate on y

        const float w_y[2] = {1.f - fAlpha_y, fAlpha_y};

        // Bilinear interp
        std::vector<float> retVal(m_n_pixel_samples, 0);

        for (size_t y = fMin_y; y <= fMax_y; y++) {
            const float weight_y = w_y[y - (size_t)fMin_y];
            
            for (size_t x = fMin_x; x <= fMax_x; x++) {
                const float weight_x = w_x[x - (size_t)fMin_x];
                const float weight = weight_x * weight_y;

                for (size_t c = 0; c < m_n_pixel_samples; c++) {
                    retVal[c] += m_image[m_n_pixel_samples * (y * m_width + x) + c] * weight;
                }
            }
        }

        for (size_t c = 0; c < m_n_pixel_samples; c++) {
            value[c] = (T)(retVal[c]);
        }
    }


    size_t width() const {
        return m_width;
    }


    size_t height() const {
        return m_height;
    }

private:
    size_t m_width, m_height;
    size_t m_n_pixel_samples;

    std::vector<T> m_image;
};


void RGB_to_YCbCr(
    float  R, float  G , float  B,
    float& Y, float& Cb, float& Cr,
    float  Kb, float Kr)
{
    const float Kg = 1.f - Kb - Kr;

    Y = Kr * R + Kg * G + Kb * B;
    Cb = 0.5f * (B - Y) / (1.f - Kb);
    Cr = 0.5f * (R - Y) / (1.f - Kr);
}


void YCbCr_to_RGB(
    float  Y, float  Cb, float  Cr,
    float& R, float& G , float& B,
    float  Kb, float Kr)
{
    const float Kg = 1.f - Kb - Kr;

    R = Y                                 +         (2.f - 2.f * Kr) * Cr;
    G = Y - Kb/Kg * (2.f - 2.f * Kb) * Cb - Kr/Kg * (2.f - 2.f * Kb) * Cr;
    B = Y +         (2.f - 2.f * Kb) * Cb;
}


int main(int argc, char* argv[])
{
    if (argc < 5) {
        std::cout << "Usage: " << std::endl
                  << argv[0] << " <png image in> <factor Y> <factor CbCr> <png image out> [<luma image out> <chroma image out>]"
                  << std::endl;

        return 0;
    }

    const char* filename_in  = argv[1];
    const char* filename_out = argv[4];

    const size_t downsample_Y  = atoi(argv[2]);
    const size_t downsample_Cb = atoi(argv[3]);
    const size_t downsample_Cr = atoi(argv[3]);

    // ITU-R BT.601
    const float Kb = 0.114f;
    const float Kr = 0.299f;

    /*
    // ITU-R BT.709
    const float Kb = 0.0722f;
    const float Kr = 0.2126f;

    // ITU-R BT.2020
    const float Kb = 0.0593f;
    const float Kr = 0.2627f;
    */

    // Load image
    std::vector<unsigned char> image_org;
    unsigned width, height;
    unsigned error = lodepng::decode(image_org, width, height, filename_in);

    if (error) {
        std::cout << "PNG decoder error: " << error << ": "
                  << lodepng_error_text(error) << std::endl;
        exit(-1);
    }

    // Separate luma and chroma
    std::vector<float> image_Y (width * height);
    std::vector<float> image_Cb(width * height);
    std::vector<float> image_Cr(width * height);

    for (size_t y = 0; y < height; y++) {
        for (size_t x = 0; x < width; x++) {
            const float R = image_org[4 * (y * width + x) + 0];
            const float G = image_org[4 * (y * width + x) + 1];
            const float B = image_org[4 * (y * width + x) + 2];

            // Convert to YCbCr
            RGB_to_YCbCr(
                R, G, B,
                image_Y [y * width + x], 
                image_Cb[y * width + x], 
                image_Cr[y * width + x],
                Kb, Kr);
        }
    }

    // Compression
    const Image<float> resized_Y (image_Y,  width, height, downsample_Y,  1);
    const Image<float> resized_Cb(image_Cb, width, height, downsample_Cb, 1);
    const Image<float> resized_Cr(image_Cr, width, height, downsample_Cr, 1);

    // Decompression
    std::vector<unsigned char> image_edited(4 * width * height);

    for (int y = 0; y < height; y++) {
        const float v = (float)y / (float)(height - 1);

        for (int x = 0; x < width; x++) {
            const float u = (float)x / (float)(width - 1);

            float Y, Cb, Cr;
            float R, G, B;

            resized_Y. get_pixel(u, v, &Y);
            resized_Cb.get_pixel(u, v, &Cb);
            resized_Cr.get_pixel(u, v, &Cr);
            
            // Convert to RGB
            YCbCr_to_RGB(
                Y, Cb, Cr,
                R, G, B,
                Kb, Kr
            );

            image_edited[4 * (y * width + x) + 0] = std::min(std::max(R, 0.f), 255.f);
            image_edited[4 * (y * width + x) + 1] = std::min(std::max(G, 0.f), 255.f);
            image_edited[4 * (y * width + x) + 2] = std::min(std::max(B, 0.f), 255.f);

            // Alpha from original image
            image_edited[4 * (y * width + x) + 3] = image_org[4 * (y * width + x) + 3];
        }
    }

    // Write edited image to disk
    error = lodepng::encode(filename_out, image_edited, width, height);

    if (error) {
        std::cout << "PNG encoder error: " << error << ": "
                  << lodepng_error_text(error) << std::endl; 
    }


    // Optionally, write additional luma chroma images to disk
    if (argc == 7) {
        const char* filename_luma_out   = argv[5];
        const char* filename_chroma_out = argv[6];

        std::vector<unsigned char> image_luma  (4 * width * height);
        std::vector<unsigned char> image_chroma(4 * width * height);

        for (int y = 0; y < height; y++) {
            const float v = (float)y / (float)(height - 1);

            for (int x = 0; x < width; x++) {
                const float u = (float)x / (float)(width - 1);

                // Write luma in a separate framebuffer
                float Y;

                resized_Y.get_pixel(u, v, &Y);

                for (int c = 0; c < 3; c++) {
                    image_luma[4 * (y * width + x) + c] = Y;
                }

                // Convert to RGB while leaving the luma at a constant 
                // value
                Y = 128.f;
                float Cb, Cr;
                float R, G, B;

                resized_Cb.get_pixel(u, v, &Cb);
                resized_Cr.get_pixel(u, v, &Cr);

                // Convert to RGB
                YCbCr_to_RGB(
                    Y, Cb, Cr,
                    R, G, B,
                    Kb, Kr
                );

                image_chroma[4 * (y * width + x) + 0] = std::min(std::max(R, 0.f), 255.f);
                image_chroma[4 * (y * width + x) + 1] = std::min(std::max(G, 0.f), 255.f);
                image_chroma[4 * (y * width + x) + 2] = std::min(std::max(B, 0.f), 255.f);

                // Alpha from original image
                image_luma  [4 * (y * width + x) + 3] = image_org[4 * (y * width + x) + 3];
                image_chroma[4 * (y * width + x) + 3] = image_org[4 * (y * width + x) + 3];
            }
        }

        // Write luma and chroma images to disk
        error = lodepng::encode(filename_luma_out, image_luma, width, height);

        if (error) {
            std::cout << "PNG encoder error: " << error << ": "
                    << lodepng_error_text(error) << std::endl; 
        }

        error = lodepng::encode(filename_chroma_out, image_chroma, width, height);

        if (error) {
            std::cout << "PNG encoder error: " << error << ": "
                    << lodepng_error_text(error) << std::endl; 
        }
    }

    return 0;
}
